# A **F**inite **S**tate **M**achine in Java.

The idea of a FSM is that you define a set of states and events that transition between them, so that the state of the FSM is well-defined, and it is impossible for the FSM to get into an unknown, undefined, state. As well as state a FSM can also modify data associated with it in a controlled way as it changes state.

_This is not a library. You may use the source code (5 java files + unit tests) as you see fit._

This implementation consists of 2 abstract generic typed classes...
* `AsynchronousFiniteStateMachineBase` 
* `FiniteStateMachineBase` 

...that you extend to derive your concrete FSM class. 
Both require you to provide the following...

* **Generic Types**
1. `<S>` States - an enum type that defines all the FSMs states.
2. `<E>` Events - a base class for the classes that represent the event type, (could be Object).
3. `<D>` Data type - the class that represents the data associated with the FSM.
4. `<ID>` Immutable access to that data - an interface or abstract class that only exposes the data in a read-only way that cannot modify the data.

* **Constructor Arguments**
1. `initialState` - An initial state. The state is defined by an enum.
2. `data` - The instance of the data associated with the FSM which optionally can be modified by the FSM via the `mutate` method in the state transition sequence.
3. `builderCallback` - Allows you to define the sequence of state transitions from events.

For the abstract class `FiniteStateMachineBase`, the defined state change events fired against this are executed synchronously and immediately.
```java
boolean success = fsm.fireEvent(new myEvent());
```

The abstract class `AsynchronousFiniteStateMachineBase` extends `FiniteStateMachineBase` and adds the `fireEventAsync` method. This method fires the event asynchronously by putting the event into a queue with an optional callback when completed. Events are taken from the queue and fired against the FSM by a separate thread in the order they were added, until the queue is empty.
```java
fsm.fireEventAsync(new myEvent(), (success, event, preSate, postSate) -> 
    System.out.println("from " + preSate + " to " + postSate + " with event " + event.getClass().getSimpleName() + ". success = " + success)
);
```
The FSM can be used to modify data during a valid transition with the `mutate` method in the builder sequence.
The FSM attempts to restrict mutable access to this data in other places by using the immutable interface `<ID>` to your data.

For usage examples see the unit tests, especially `CoinOpTurnstileFSMTest` which is a simple example of a FSM based on a coin operated turnstile.

### Example
This following example is taken from `CoinOpTurnstileFSMTest` and uses a FSM based on `FiniteStateMachineBase`

**Define the Types**
```java
// States
enum TurnstileStates {
    Locked, Unlocked
}

// Events
class Coin {}
class Push {}

// Immutable Data Interface
interface ImmutableData {
    long getCount();
}

// The Data (mutable)
class Data implements ImmutableData {
    long count = 0;

    @Override
    public long getCount() {
        return count;
    }
}
```
**Define the FSM concrete class and populate the constructor arguments**
```java
class CoinOpTurnstileFiniteStateMachine extends FiniteStateMachineBase<
        TurnstileStates, // The state enum class.
        Object, // The event base class.
        Data, // The data class.
        ImmutableData> { // The immutable interface to the data.
    
    public CoinOpTurnstileFiniteStateMachine() {
        super(TurnstileStates.Locked, // The initial state.
              new Data(), // The data associated with the FSM
              (builder) -> builder 
                .when(Coin.class) // when this Coin event is fired against this FSM..do the following transition..
                .transition("coin to unlock") // name the transition
                    .from(TurnstileStates.Locked) // from Locked state
                    .to(TurnstileStates.Unlocked) // to Unlocked state
                    .end() // end of transition.
    
                .when(Push.class) // when this Push event is fired against this FSM..do the following transition..
                .transition("push on unlocked to lock") // name the transition
                    .from(TurnstileStates.Unlocked) // from Unlocked state
                    .to(TurnstileStates.Locked) // to Locked state
                    .mutate((data) -> data.count++) // count people through the turnstile.
                    .end() // end of transition.
        );
    }
}
```
So now the FSM will only be able to transition from Locked to Unlocked if Coin event is fired against the FSM in the Locked state. And the FSM will only be able to transition from Unlocked to locked if Push event is fired against the FSM in the Unlocked state. For anything else the `fireEvent` will return `false` and the FSM will not have changed state.

**Using the coin operated turnstile FSM...**
```java
assert coinOpTurnstileFSM.getCurrentState() == TurnstileStates.Locked;
coinOpTurnstileFSM.fireEvent(new Coin()); // will return true - changed state from Locked to Unlocked
assert coinOpTurnstileFSM.getCurrentState() == TurnstileStates.Unlocked;
coinOpTurnstileFSM.fireEvent(new Coin()); // will return false - no change to state
assert coinOpTurnstileFSM.getCurrentState() == TurnstileStates.Unlocked;
coinOpTurnstileFSM.fireEvent(new Push()); // will return true - changed state from Unocked to Locked
assert coinOpTurnstileFSM.getCurrentState() == TurnstileStates.Locked;
```

The methods on the builder interfaces (provided by `builderCallback`) to define the transition are...

* `when([Event Class])` - starts the transition definition when an event object of type `[Event class]` is fired against the FSM.
* `when([Event Class], [Precondition])` - starts the transition definition when an event object of type `[Event class]` is fired against the FSM AND a precondition is meet, defined by a predicate with the event as a parameter.
* `transition([Transition Description])` - names the transition definition and start the transition definition sequence. If the `[Transition Description]` has previously been used then that sequence is associated with this event, so you don't need to duplicate transition definition sequences for when different events have the same sequence.
* `from([From Enum State])` - defines the _from_ state in the transition sequence.
* `to([To Enum State])` - defines the _to_ state in the transition sequence.
* `action([Funtion taking ActionParams])` - defines an action that can be executed in the sequence with access to the immutable data and current state of the FSM via `ActionParams`.

E.g. Use of `action` and `ActionParams`
```java
.action((ap) -> {
    System.out.println("Immutable Data: " + ap.get().getCount() +
                       ".State from: " + ap.getFrom() +
                       ".State to: " + ap.getTo() +
                       ".Event: " + ap.getEvent().getClass().getSimpleName() +
                       ".State now: " + ap.getNow());
    return true; // true to continue with transition.
})
```

For more information on Finite State Machines...
* https://brilliant.org/wiki/finite-state-machines/
* https://medium.com/datadriveninvestor/state-machine-design-pattern-why-how-example-through-spring-state-machine-part-1-f13872d68c2d
