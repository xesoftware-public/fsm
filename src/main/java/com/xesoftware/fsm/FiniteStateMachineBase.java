package com.xesoftware.fsm;

import lombok.NonNull;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Implementation of a Finite State Machine.
 * Abstract class, extend to concrete class to implement transition sequences with
 * builder provided to constructor.
 * <p>
 * S is state
 * D is data
 * E is event
 * ID is immutable data (e.g parent interface of D without any setters)
 */
public abstract class FiniteStateMachineBase<S, E, D extends ID, ID>
        implements FiniteStateMachine<S, E, ID> {

    /**
     * The Action parameters interface.
     * @param <S> the state.
     * @param <E> the event.
     * @param <ID> the immutable data.
     */
    interface ActionParams<S, E, ID> extends Supplier<ID> {
        E getEvent(); // gets the event.
        S getFrom(); // gets the from state.
        S getTo(); // gets the to state.
        S getNow(); // gets the state now.
        ID get(); // gets the immutable data.
    }

    /**
     * the Action interface
     * @param <S> the state.
     * @param <E> the event.
     * @param <ID> the immutable data.
     */
    interface Action<S, E, ID> {
        boolean execute(ActionParams<S, E, ID> actionParams);
    }

    /**
     * the Mutate interface.
     * @param <D> the mutable data.
     */
    interface Mutate<D> {
        void mutate(D data);
    }

    /**
     * the command interface.
     * @param <E> the event.
     */
    private interface Command<E> {
        boolean execute(E event);
    }

    /**
     * class to represent the transition sequence.
     * @param <S> the state type.
     * @param <E> the event type.
     */
    static private class Sequence<S, E> {
        final List<Command<E>> processes = new ArrayList<>();
        String transitionName;
        S fromState;
        S toState;
    }

    /**
     * class to represent the action parameter values
     * @param <S> the state type.
     * @param <E> the event type.
     * @param <ID> the immutable data type.
     */
    private static class ActionParamsImpl<S, E, ID> implements ActionParams<S, E, ID> {
        final private E event;
        final private S from;
        final private S to;
        final private S now;
        final private Supplier<ID> immutableDataSupplier;

        private ActionParamsImpl(final E event,
                                 final S from,
                                 final S to,
                                 final S now,
                                 final Supplier<ID> immutableDataSupplier) {
            this.event = event;
            this.from = from;
            this.to = to;
            this.now = now;
            this.immutableDataSupplier = immutableDataSupplier;
        }

        @Override
        public E getEvent() {
            return event;
        }

        @Override
        public S getFrom() {
            return from;
        }

        @Override
        public S getTo() {
            return to;
        }

        @Override
        public S getNow() {
            return now;
        }

        @Override
        public ID get() {
            return immutableDataSupplier.get();
        }
    }

    /**
     * class to represent the event transitions
     * @param <S> the state type.
     * @param <E> the event type.
     */
    private static class EventTransitions<S, E> {
        final String eventKey;
        final Predicate<E> predicate;

        // map from state to end state with sequences
        final Map<S, Sequence<S, E>> sequences = new LinkedHashMap<>();

        private EventTransitions(final String eventKey,
                                 final Predicate<E> predicate) {
            this.eventKey = eventKey;
            this.predicate = predicate;
        }

        private boolean isPredicate(final E event) {
            boolean ret = true;
            if (null != predicate) {
                ret = predicate.test(event);
            }
            return ret;
        }
    }

    /**
     * class to represent the command - change state
     * @param <S> the state type.
     * @param <E> the event type.
     * @param <D> the mutable data type.
     * @param <ID> the immutable data type.
     */
    private static class CommandChangeState<S, E, D extends ID, ID> implements Command<E> {
        final Sequence<S, E> sequence;
        final FiniteStateMachineBase<S, E, D, ID> fsm;

        public CommandChangeState(final FiniteStateMachineBase<S, E, D, ID> fsm, Sequence<S, E> sequence) {
            this.fsm = fsm;
            this.sequence = sequence;
        }

        @Override
        public boolean execute(final E event) {
            fsm.currentState = sequence.toState;
            return true;
        }
    }

    /**
     * class to represent the command - action
     * @param <S> the state type.
     * @param <E> the event type.
     * @param <D> the mutable data type.
     * @param <ID> the immutable data type.
     */
    private static class CommandAction<S, E, D extends ID, ID> implements Command<E> {
        private final Sequence<S, E> sequence;
        private final FiniteStateMachineBase<S, E, D, ID> fsm;
        private final Action<S, E, ID> action;

        private CommandAction(final FiniteStateMachineBase<S, E, D, ID> fsm,
                              final Sequence<S, E> sequence, Action<S, E, ID> action) {
            this.fsm = fsm;
            this.sequence = sequence;
            this.action = action;
        }

        @Override
        public boolean execute(final E event) {
            final ActionParams<S, E, ID> actionParams = new ActionParamsImpl<>(
                    event,
                    sequence.fromState,
                    sequence.toState,
                    fsm.getCurrentState(),
                    fsm);
            return action.execute(actionParams);
        }
    }

    /**
     * class to represent the command - mutate
     * @param <S> the state type.
     * @param <E> the event type.
     * @param <D> the mutable data type.
     * @param <ID> the immutable data type.
     */
    private static class CommandMutate<S, E, D extends ID, ID> implements Command<E> {
        private final FiniteStateMachineBase<S, E, D, ID> fsm;
        private final Mutate<D> mutate;

        private CommandMutate(final FiniteStateMachineBase<S, E, D, ID> fsm,
                              final Mutate<D> mutate) {
            this.fsm = fsm;
            this.mutate = mutate;
        }

        @Override
        public boolean execute(final E event) {
            fsm.mutate(mutate);
            return true;
        }
    }

    ////////////////////////////////// The Finite State Machine Builder ////////////////////////////////////////////////

    interface BuilderWhen<S, E, D, ID> {
        BuilderFrom<S, E, D, ID> when(Class<? extends E> eventClass);

        BuilderFrom<S, E, D, ID> when(Class<? extends E> eventClass, Predicate<E> predicate);
    }

    interface BuilderGo<S, E, D, ID> extends BuilderWhen<S, E, D, ID>, BuilderFrom<S, E, D, ID> {
    }

    interface BuilderFrom<S, E, D, ID> {
        BuilderGo<S, E, D, ID> transition(String name);

        Builder<S, E, D, ID> from(S state);
    }

    interface Builder<S, E, D, ID> {
        /**
         * to state transition.
         * @param state the to state.
         * @return the builder
         */
        Builder<S, E, D, ID> to(S state);

        /**
         * the action to perform in the transition sequence.
         * Has access to immutable state data.
         * @param action the action.
         * @return the builder.
         */
        Builder<S, E, D, ID> action(Action<S, E, ID> action);

        /**
         * the mutation to perform on the state data.
         * Has access to mutable state data.
         * This is the only way to modify the state of the fsm.
         * @param mutate the mutation
         * @return the builder.
         */
        Builder<S, E, D, ID> mutate(Mutate<D> mutate);

        /**
         * ends the transition sequence.
         * @return the builder
         */
        BuilderGo<S, E, D, ID> end();
    }

    /**
     * The builder implementation to define the transition sequence.
     */
    private class BuilderImpl implements
            Builder<S, E, D, ID>,
            BuilderFrom<S, E, D, ID>,
            BuilderWhen<S, E, D, ID>,
            BuilderGo<S, E, D, ID> {
        private final Map<String, List<EventTransitions<S, E>>> eventTransitionsMap = new HashMap<>();
        private final Map<String, Sequence<S, E>> nameToSequenceMap = new HashMap<>();
        private String transitionName;
        private EventTransitions<S, E> eventTransitions;
        private Sequence<S, E> sequence;

        private Map<String, List<EventTransitions<S, E>>> getEventTransitionsMap() {
            return eventTransitionsMap;
        }

        @Override
        public BuilderFrom<S, E, D, ID> when(@NonNull Class<? extends E> eventClass) {
            return when(eventClass, null);
        }

        @Override
        public BuilderFrom<S, E, D, ID> when(@NonNull final Class<? extends E> eventClass,
                                             final Predicate<E> predicate) {
            if (Modifier.isAbstract(eventClass.getModifiers())) {
                throw new IllegalStateException("Abstract class " + eventClass.getName() + " passed into builders when method. Transitions triggered from concrete event classes only");
            }

            return when(eventClass.getName(), predicate);
        }

        private BuilderFrom<S, E, D, ID> when(final String eventKey,
                                              final Predicate<E> predicate) {
            eventTransitions = new EventTransitions<>(eventKey, predicate);
            final List<EventTransitions<S, E>> eventTransitionsList = eventTransitionsMap.computeIfAbsent(eventKey, k -> new ArrayList<>());
            eventTransitionsList.add(eventTransitions);
            return this;
        }

        public Builder<S, E, D, ID> from(@NonNull final S fromState) {
            sequence = new Sequence<>();
            sequence.transitionName = transitionName;
            sequence.fromState = fromState;
            return this;
        }

        @Override
        public BuilderGo<S, E, D, ID> transition(@NonNull final String name) {
            Objects.requireNonNull(name, "transition must have a name"); // throws NullPointerException if name is null
            transitionName = name;
            sequence = nameToSequenceMap.get(name);

            if (null != sequence) {
                end();
            }

            return this;
        }

        public Builder<S, E, D, ID> to(@NonNull final S toState) {
            if (null == sequence) throw new IllegalStateException("builder error: to must come after from");

            sequence.toState = toState;
            sequence.processes.add(new CommandChangeState<>(FiniteStateMachineBase.this, sequence));
            return this;
        }

        public Builder<S, E, D, ID> action(@NonNull final Action<S, E, ID> action) {
            if (null == sequence) throw new IllegalStateException("builder error: action must come after from");

            sequence.processes.add(new CommandAction<>(FiniteStateMachineBase.this, sequence, action));
            return this;
        }

        @Override
        public Builder<S, E, D, ID> mutate(@NonNull final Mutate<D> mutate) {
            if (null == sequence) throw new IllegalStateException("builder error: mutate must come after from");

            sequence.processes.add(new CommandMutate<>(FiniteStateMachineBase.this, mutate));
            return this;
        }

        @Override
        public BuilderGo<S, E, D, ID> end() {
            if (null == sequence) throw new IllegalStateException("builder error: end must come after from");
            if (null == sequence.toState) throw new IllegalStateException("builder error: sequence must define to");

            final Sequence<S, E> existingSequence = eventTransitions.sequences.get(sequence.fromState);
            if (null == existingSequence) {
                eventTransitions.sequences.put(sequence.fromState, sequence);
            } else {
                throw new IllegalStateException("Transition from " + sequence.fromState + " to " + sequence.toState + " already exists!");
            }

            nameToSequenceMap.put(sequence.transitionName, sequence);

            sequence = null;
            transitionName = null;
            return this;
        }
    }

    ///////////////////////////////////////////////////////////////////////////

    private final D data;
    private final Map<String, List<EventTransitions<S, E>>> eventTransitionsMap;
    private S currentState;

    protected FiniteStateMachineBase(@NonNull final S initialState,
                                     @NonNull final D data,
                                     @NonNull final Consumer<BuilderWhen<S, E, D, ID>> builderCallback) {
        final BuilderImpl builder = new BuilderImpl();
        builderCallback.accept(builder);
        eventTransitionsMap = builder.getEventTransitionsMap();
        currentState = initialState;
        this.data = data;
    }

    @Override
    public S getCurrentState() {
        return currentState;
    }

    @Override
    public ID get() {
        return data;
    }

    /**
     * gets the event key, which is by default the event class name.
     * @param event the event.
     * @return the event key.
     */
    private String getEventKey(final E event) {
        return event.getClass().getName();
    }

    /**
     * mutates, modifies, the data.
     * @param mutate interface to mutate the data.
     */
    private void mutate(final Mutate<D> mutate) {
        synchronized (data) {
            mutate.mutate(data);
        }
    }

    @Override
    public boolean fireEvent(@NonNull final E event) {
        return fireEvent(getEventKey(event), event);
    }

    private synchronized boolean fireEvent(final String eventKey, final E event) {
        boolean success = false;
        final List<EventTransitions<S, E>> eventTransitions = eventTransitionsMap.get(eventKey);
        if (null != eventTransitions) {
            // get all the event transitions for the event key.
            for (EventTransitions<S, E> eventTransition : eventTransitions) {
                // do any have sequences that start from the current state.
                final Sequence<S, E> sequence = eventTransition.sequences.get(getCurrentState());
                if (null != sequence) {
                    // does the event transition predicate allow it...
                    if (eventTransition.isPredicate(event)) {
                        // execute sequence commands until command returns false or all executed successfully.
                        success = sequence.processes.stream().allMatch(c -> c.execute(event));
                        break;
                    }
                }
            }
        }
        return success;
    }
}
