package com.xesoftware.fsm;

public interface AsyncFiniteStateMachine<S, E, ID> extends FiniteStateMachine<S, E, ID> {
    void fireEventAsync(final E event, final AsynchronousFiniteStateMachineBase.Callback<S, E> callback);
}
