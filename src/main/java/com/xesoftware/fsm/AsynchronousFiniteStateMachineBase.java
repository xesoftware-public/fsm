package com.xesoftware.fsm;

import lombok.NonNull;

import java.util.function.Consumer;

/**
 * Adds a Asynchronous fireEventAsync method that changes the state of the fsm asynchronously
 *
 * S is state
 * D is data (extends ID)
 * E is event
 * ID is immutable data
 */
public abstract class AsynchronousFiniteStateMachineBase<S, E, D extends ID, ID>
        extends FiniteStateMachineBase<S, E, D, ID>
        implements AsyncFiniteStateMachine<S, E, ID> {

    /**
     * callback interface, onComplete called when action event execution completed.
     */
    @FunctionalInterface
    interface Callback<S, E> {
        void onComplete(boolean success, E event, S preState, S postSate);
    }

    /**
     * The Runnable to execute a command event.
     */
    private static class ExecuteCommandEvent<S, E, D extends ID, ID> implements Runnable {
        final FiniteStateMachineBase<S, E, D, ID> fsm;
        final E event;
        final Callback<S, E> callback;

        /**
         * constructor.
         * @param fsm The Finite State Machine.
         * @param event The event.
         * @param callback The callback (or null)
         */
        private ExecuteCommandEvent(final FiniteStateMachineBase<S, E, D, ID> fsm,
                                    final E event,
                                    final Callback<S, E> callback) {
            this.fsm = fsm;
            this.event = event;
            this.callback = callback;
        }

        @Override
        public void run() {
            final S preState = fsm.getCurrentState();
            final boolean ret = fsm.fireEvent(event);
            final S postState = fsm.getCurrentState();
            if (null != callback) {
                callback.onComplete(ret, event, preState, postState);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////

    // The executor queue
    private final ExecutorQueue executorQueue = new ExecutorQueue();

    /**
     * constructor for the AsynchronousFiniteStateMachineBase
     * @param initialState The initial state.
     * @param data The data to mutate
     * @param builderCallback The builder to define the transitions.
     */
    protected AsynchronousFiniteStateMachineBase(final S initialState,
                                                 final D data,
                                                 final Consumer<BuilderWhen<S, E, D, ID>> builderCallback) {
        super(initialState, data, builderCallback);
    }

    /**
     * fires event asynchronously, i.e. fireEvent returns immediately and event is put on queue
     * @param event the event.
     * @param callback must be null (no callback) or a callback
     */
    public void fireEventAsync(@NonNull final E event, final Callback<S, E> callback) {
        executorQueue.put(new ExecuteCommandEvent<>(this, event, callback));
    }
}
