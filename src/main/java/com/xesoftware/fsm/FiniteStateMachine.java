package com.xesoftware.fsm;

import java.util.function.Supplier;

/**
 * Finite State Machine interface
 */
public interface FiniteStateMachine<S, E, ID> extends Supplier<ID> {
    /**
     * gets the current state of the finite machine.
     */
    S getCurrentState();

    /**
     * fires an event on the Finite State Machine to change its state.
     * @param event The event to fire.
     * @return true if event was successful.
     */
    boolean fireEvent(E event);

    /**
     * gets an immutable (read-only) interface to the data associated with this FSM.
     * @return immutable data
     */
    ID get();
}
