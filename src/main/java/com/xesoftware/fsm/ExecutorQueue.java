package com.xesoftware.fsm;

import lombok.NonNull;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Used by AsynchronousFiniteStateMachineBase to process events asynchronously.
 * Events are added to the ExecutorQueue from potentially multiple threads
 * then each event is executed once in the order added,
 * then removed from ExecutorQueue.
 */
class ExecutorQueue {

    /**
     * runs the runnable entries in the queue.
     */
    private class QueueRunner implements Runnable {

        private final AtomicBoolean running = new AtomicBoolean(true);

        @Override
        public void run() {
            while (running.get()) {
                Runnable e;

                // synchronize queue access.
                synchronized (queue) {
                    if (queue.isEmpty()) {
                        running.set(false);
                        break; // queue empty, leave loop, queue runner done.
                    } else {
                        e = queue.remove();
                    }
                }

                e.run();
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    private final Queue<Runnable> queue = new LinkedList<>();
    private Thread executionThread = null;
    private QueueRunner queueRunner = null;

    /**
     * puts a runnable command in the queue.
     * returns immediately.
     * @param command the runnable command.
     */
    void put(@NonNull final Runnable command) {
        synchronized (queue) {
            queue.add(command);
            if (null == executionThread || !executionThread.isAlive()) {
                executionThread = createExecutionThread();
                executionThread.start();
            }
        }
    }

    /**
     * stops the queue runner thread, by setting flag
     */
    void stop() {
        if (null != queueRunner) {
            queueRunner.running.set(false);
        }
    }

    /**
     * create a new execution thread after stopping any previous ones.
     * @return the new execution thread
     */
    private Thread createExecutionThread() {
        stop();
        queueRunner = new QueueRunner();
        return new Thread(queueRunner);
    }
}
