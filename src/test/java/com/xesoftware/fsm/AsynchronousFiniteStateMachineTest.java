package com.xesoftware.fsm;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.xesoftware.fsm.TestConfigFiniteStateMachine.*;

/**
 * Tests the AsynchronousFiniteStateMachine class.
 */
public class AsynchronousFiniteStateMachineTest {
    TestAsynchronousFiniteStateMachine test;

    private boolean processAction(String id, FiniteStateMachineBase.ActionParams<TestStates, SomeEvent, ImmutableTestData> params) {
        System.out.println("Id=" + id + ". Event=" + params.getEvent().getMyEventName() + ". state=" + params.getNow() + ". data=" + params.get());
        return true;
    }

    @Before
    public void setUp() {
        test = new TestAsynchronousFiniteStateMachine();
    }

    @Test
    public void getCurrentState() {
        Assert.assertNotNull(test.getCurrentState());
    }

    @Test
    public void getImmutableData() {
        Assert.assertNotNull(test.get());
    }

    @Test
    public void fireEvent_Async() {
        testAsyncCallback(new SomeEvent1(TestStates.State1, TestStates.State2, 1));
        testAsyncCallback(new SomeEvent1(TestStates.State2, TestStates.State3, 2));
        testAsyncCallback(new SomeEvent1(TestStates.State3, TestStates.State1, 3));
        testAsyncCallback(new SomeEvent2(TestStates.State1, TestStates.State2, 4));
        testAsyncCallback(new SomeEvent2(TestStates.State2, TestStates.State3, 5));
        testAsyncCallback(new SomeEvent2(TestStates.State3, TestStates.State1, 6));
        testAsyncCallback(new SomeEvent3(TestStates.State1, TestStates.State2, 7));
        testAsyncCallback(new SomeEvent3(TestStates.State2, TestStates.State2, 8));
        testAsyncCallback(new SomeEvent3(TestStates.State2, TestStates.State2, 9));
    }

    @Test
    public void fireEvent_Async_NoCallback() throws InterruptedException {
        testAsyncNoCallback(new SomeEvent1(TestStates.State1, TestStates.State2, 1));
        testAsyncNoCallback(new SomeEvent1(TestStates.State2, TestStates.State3, 2));
        testAsyncNoCallback(new SomeEvent1(TestStates.State3, TestStates.State1, 3));
        testAsyncNoCallback(new SomeEvent2(TestStates.State1, TestStates.State2, 4));
        testAsyncNoCallback(new SomeEvent2(TestStates.State2, TestStates.State3, 5));
        testAsyncNoCallback(new SomeEvent2(TestStates.State3, TestStates.State1, 6));
        testAsyncNoCallback(new SomeEvent3(TestStates.State1, TestStates.State2, 7));
        testAsyncNoCallback(new SomeEvent3(TestStates.State2, TestStates.State2, 8));
        testAsyncNoCallback(new SomeEvent3(TestStates.State2, TestStates.State2, 9));

        // wait a second for the events to be processed
        Thread.sleep(1000);

        Assert.assertEquals(TestStates.State2, test.getCurrentState());
    }

    @Test
    public void fireEvent_Sync() {
        testSync(new SomeEvent1(TestStates.State1, TestStates.State2, 1));
        testSync(new SomeEvent1(TestStates.State2, TestStates.State3, 2));
        testSync(new SomeEvent1(TestStates.State3, TestStates.State1, 3));
        testSync(new SomeEvent2(TestStates.State1, TestStates.State2, 4));
        testSync(new SomeEvent2(TestStates.State2, TestStates.State3, 5));
        testSync(new SomeEvent2(TestStates.State3, TestStates.State1, 6));
        testSync(new SomeEvent3(TestStates.State1, TestStates.State2, 7));
        testSync(new SomeEvent3(TestStates.State2, TestStates.State2, 8));
        testSync(new SomeEvent3(TestStates.State2, TestStates.State2, 9));
    }

    private void testAsyncCallback(SomeEvent someEvent) {
        test.fireEventAsync(someEvent, (success, event, preSate, postSate) -> {
            System.out.println("from " + preSate + " to " + postSate + " with event " + event.getClass().getSimpleName() + ". success = " + success + ". data = " + test.get().getCount());
            System.out.println();

            Assert.assertEquals(event.expectedStartState, preSate);
            Assert.assertEquals(event.expectedEndState, postSate);
        });
    }

    @Test(expected = NullPointerException.class)
    public void fireEvent_Null_Event() {
        test.fireEventAsync(null, null);
    }

    private void testAsyncNoCallback(SomeEvent someEvent) {
        test.fireEventAsync(someEvent,null);
    }

    private void testSync(SomeEvent someEvent) {
        Assert.assertEquals(someEvent.expectedStartState, test.getCurrentState());
        test.fireEvent(someEvent);
        Assert.assertEquals(someEvent.expectedEndState, test.getCurrentState());
    }

    class TestAsynchronousFiniteStateMachine extends AsynchronousFiniteStateMachineBase<TestStates, SomeEvent, TestTestData, ImmutableTestData> {
        public TestAsynchronousFiniteStateMachine() {
            super(TestStates.State1, new TestTestData(), (builder) -> builder
                .when(SomeEvent1.class, e -> e instanceof SomeEvent1)
                .transition("transition 1")
                .from(TestStates.State1)
                .action(params -> processAction("pre-1", params))
                .to(TestStates.State2)
                .action(params -> processAction("post-2", params))
                .mutate((testData) -> testData.count++)
                .action(params -> processAction("post-3", params))
                .end()

                .transition("transition 2")
                .from(TestStates.State2)
                .to(TestStates.State3)
                .end()

                .transition("transition 3")
                .from(TestStates.State3)
                .action(params -> processAction("pre-4", params))
                .mutate((testData) -> testData.count--)
                .to(TestStates.State1)
                .action(params -> processAction("post-5", params))
                .mutate((testData) -> testData.count++)
                .action(params -> processAction("post-6", params))
                .end()
                .when(SomeEvent2.class)
                .transition("transition 1")
                .transition("transition 2")
                .transition("transition 3")
                .when(SomeEvent3.class)
                .transition("transition 1")
            );
        }
    }
}
