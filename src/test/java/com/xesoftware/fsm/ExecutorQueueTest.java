package com.xesoftware.fsm;

import org.junit.Assert;
import org.junit.Test;

import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Phil on 24/05/2016.
 */
public class ExecutorQueueTest {

    ExecutorQueue test;
    final Vector<String> stringList = new Vector<>();

    @org.junit.Before
    public void setUp() {
        test = new ExecutorQueue();
    }

    @org.junit.Test
    public void put() throws Exception {
        final int numThreads = 4;
        final int numTestInThread = 10;
        Thread[] threads = new Thread[numThreads];
        for (int t = 0; t < threads.length; t++) {
            char prefix = (char) ('a' + (char) t);
            threads[t] = new Thread(() ->
            {
                for (int i = 0; i < numTestInThread; i++) {
                    test.put(new Executor(new EventTest("(" + prefix + ")" + System.currentTimeMillis())));
                }
            }
            );
        }

        for (Thread thread : threads) {
            thread.start();
        }

        boolean alive = true;

        while (alive) {
            for (int t = 0; t < threads.length && alive; t++) {
                alive = threads[t].isAlive();
            }

            if (alive) {
                System.out.println("waiting to finish");
                Thread.sleep(1000);
            }
        }

        System.out.println("size = " + stringList.size());
        stringList.forEach(System.out::println);

        Assert.assertEquals(numThreads * numTestInThread, stringList.size());
    }

    static class EventTest {
        final String id;

        public EventTest(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return id;
        }
    }

    class Executor implements Runnable {
        final EventTest param;

        public Executor(EventTest param) {
            this.param = param;
        }

        @Override
        public void run() {
            stringList.add(param.id);
        }
    }

    @Test(expected = NullPointerException.class)
    public void put_Null_Arg_Test() {
        this.test.put(null);
    }

    @Test
    public void stop_Thread_Start_Test() throws InterruptedException {
        final AtomicInteger atomicInteger = new AtomicInteger(0);
        final Runnable r = new Runnable() {
            @Override
            public void run() {
                atomicInteger.incrementAndGet();
            }
        };
        this.test.put(r); // start the thread by putting runnable in queue
        Thread.sleep(100);
        this.test.stop(); // stop the thread after r has run.
        Thread.sleep(100);
        this.test.put(r); // re-start with new thread.
        Thread.sleep(100); // wait till ran.

        Assert.assertEquals(2, atomicInteger.get());
    }

    @Test
    public void stop_Thread_BeforeFinished_Test() {
        final AtomicInteger atomicInteger = new AtomicInteger(0);
        final Runnable r = new Runnable() {
            @Override
            public void run() {
                atomicInteger.incrementAndGet();
            }
        };
        this.test.put(r); // start the thread by putting 1st runnable in queue
        this.test.put(r); // subsequent runnables
        this.test.put(r);
        this.test.put(r);
        this.test.put(r);
        this.test.stop(); // stop the thread, before all 5 runnables have run.

        // System.out.println("Count = " + atomicInteger.get());
        Assert.assertNotEquals(5, atomicInteger.get());
    }

}
