package com.xesoftware.fsm;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Implementation of Coin Operated Turnstile as documented
 * https://brilliant.org/wiki/finite-state-machines/
 */
public class CoinOpTurnstileFSMTest {

    // States
    enum TurnstileStates {
        Locked, Unlocked
    }

    // Events
    static class Coin {}
    static class Push {}

    // Immutable Data Interface
    interface ImmutableData {
        long getCount();
    }

    // The Data (mutable)
    static class Data implements ImmutableData {
        long count = 0;

        @Override
        public long getCount() {
            return count;
        }
    }

    static class CoinOpTurnstileFiniteStateMachine extends FiniteStateMachineBase<
            TurnstileStates,
            Object,
            Data,
            ImmutableData> {

        public CoinOpTurnstileFiniteStateMachine() {
            super(TurnstileStates.Locked, new Data(), (builder) -> builder

                .when(Coin.class)
                .transition("coin to unlock")
                    .from(TurnstileStates.Locked)
                    .to(TurnstileStates.Unlocked)
                    .end()

                .when(Push.class)
                .transition("push on unlocked to lock")
                    .from(TurnstileStates.Unlocked)
                    .to(TurnstileStates.Locked)
                    .mutate((data) -> data.count++) // count people through the turnstile.
                    .end()
            );
        }
    }

    CoinOpTurnstileFiniteStateMachine test;

    @Before
    public void setUp() {
        test = new CoinOpTurnstileFiniteStateMachine();
    }

    @Test
    public void test_Push_Coin_Push() {
        // Push against locked turnstile
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());
        Assert.assertFalse(test.fireEvent(new Push()));
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());

        // Add Coin
        Assert.assertTrue(test.fireEvent(new Coin()));
        Assert.assertEquals(TurnstileStates.Unlocked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());

        // Now we can push
        Assert.assertTrue(test.fireEvent(new Push()));
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(1, test.get().getCount());
    }

    @Test
    public void test_Push_Push_Push() {
        // Push against locked turnstile
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());
        Assert.assertFalse(test.fireEvent(new Push()));
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());

        // Push against locked turnstile
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());
        Assert.assertFalse(test.fireEvent(new Push()));
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());

        // Push against locked turnstile
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());
        Assert.assertFalse(test.fireEvent(new Push()));
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());
    }

    @Test
    public void test_Coin_Coin_Coin() {
        // put coin in locked turnstile, to unlock it.
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());
        Assert.assertTrue(test.fireEvent(new Coin()));
        Assert.assertEquals(TurnstileStates.Unlocked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());

        // try to put another coin in, should fail.
        Assert.assertFalse(test.fireEvent(new Coin()));
        Assert.assertEquals(TurnstileStates.Unlocked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());

        // try to put another coin in, should fail again.
        Assert.assertFalse(test.fireEvent(new Coin()));
        Assert.assertEquals(TurnstileStates.Unlocked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());
    }

    @Test
    public void test_Coin_Coin_Push() {
        // put coin in locked turnstile, to unlock it.
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());
        Assert.assertTrue(test.fireEvent(new Coin()));
        Assert.assertEquals(TurnstileStates.Unlocked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());

        // try to put another coin in, should fail.
        Assert.assertFalse(test.fireEvent(new Coin()));
        Assert.assertEquals(TurnstileStates.Unlocked, test.getCurrentState());
        Assert.assertEquals(0, test.get().getCount());

        // push turnstile, should succeed, and leave turnstile locked..
        Assert.assertTrue(test.fireEvent(new Push()));
        Assert.assertEquals(TurnstileStates.Locked, test.getCurrentState());
        Assert.assertEquals(1, test.get().getCount());
    }
}
