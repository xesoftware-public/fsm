package com.xesoftware.fsm;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.xesoftware.fsm.TestConfigFiniteStateMachine.*;

/**
 * Tests the finite state machine
 */
public class FiniteStateMachineTest {
    TestFiniteStateMachine test;

    static boolean processAction(final String id,
                                 final FiniteStateMachineBase.ActionParams<TestStates, SomeEvent, ImmutableTestData> params) {
        System.out.println("  Id=" + id +
                ". Event=" + params.getEvent().getMyEventName() +
                ". state now=" + params.getNow() +
                ". state from=" + params.getFrom() +
                ". state to=" + params.getTo() +
                ". data=" + params.get().getCount());
        return true;
    }

    @Before
    public void setUp() {
        test = new TestFiniteStateMachine();
    }

    @Test
    public void getCurrentState() {
        Assert.assertEquals(TestStates.State1, test.getCurrentState());
    }

    @Test
    public void getData() {
        Assert.assertNotNull(test.get());
    }

    @Test
    public void fireEvent() {
        t(new SomeEvent1(TestStates.State1, TestStates.State2, 1), 1);
        t(new SomeEvent1(TestStates.State2, TestStates.State3, 2), 1);
        t(new SomeEvent1(TestStates.State3, TestStates.State1, 3), 1);
        t(new SomeEvent2(TestStates.State1, TestStates.State2, 4), 2);
        t(new SomeEvent2(TestStates.State2, TestStates.State3, 5), 2);
        t(new SomeEvent2(TestStates.State3, TestStates.State1, 6), 2);
        t(new SomeEvent3(TestStates.State1, TestStates.State2, 7), 3);
        t(new SomeEvent3(TestStates.State2, TestStates.State2, 8), 3);
        t(new SomeEvent3(TestStates.State2, TestStates.State2, 9), 3);
        t(new SomeEvent3(TestStates.State2, TestStates.State3, 10), 3);
    }

    @Test(expected = NullPointerException.class)
    public void fireEvent_Fail_Null_Event() {
        test.fireEvent(null);
    }

    @Test
    public void fireEvent_Fail_No_Valid_Transition_Defined() {
        Assert.assertFalse(test.fireEvent(new SomeEvent4(TestStates.State2, TestStates.State1, 1)));
    }

    @Test
    public void fireEvent_Fail_No_Transition() {
        Assert.assertEquals(TestStates.State1, test.getCurrentState());
        Assert.assertFalse(test.fireEvent(new SomeEvent0(TestStates.State1, TestStates.State2, 0)));
    }

    @Test
    public void fireEvent_Fail_No_Sequence() {
        Assert.assertEquals(TestStates.State1, test.getCurrentState());
        Assert.assertFalse(test.fireEvent(new SomeEvent4(TestStates.State1, TestStates.State2, 0)));
    }

    @Test
    public void fireEvent_Fail_Action_Failed() {
        Assert.assertEquals(TestStates.State1, test.getCurrentState());
        test.fireEvent(new SomeEvent1(TestStates.State1, TestStates.State2, 1));
        test.fireEvent(new SomeEvent2(TestStates.State2, TestStates.State3, 2));
        Assert.assertEquals(TestStates.State3, test.getCurrentState());
        Assert.assertFalse(test.fireEvent(new SomeEvent4(TestStates.State3, TestStates.State3, 0)));
    }

    private void t(final SomeEvent event, final int data) {
        System.out.println("" + event.expectedStartState +
                " to " + event.expectedEndState +
                " with event " + event.getClass().getSimpleName());

        TestStates state1 = test.getCurrentState();
        boolean success = test.fireEvent(event);
        TestStates state2 = test.getCurrentState();
        int count = test.get().getCount();

        System.out.println("success = " + success + ". data = " + count);
        System.out.println();

        Assert.assertEquals(event.expectedStartState, state1);
        Assert.assertEquals(event.expectedEndState, state2);
        Assert.assertEquals(data, count);
    }

    static class TestFiniteStateMachine extends FiniteStateMachineBase<TestStates, SomeEvent, TestTestData, ImmutableTestData> {
        public TestFiniteStateMachine() {
            super(TestStates.State1, new TestTestData(), (builder) -> builder
                .when(SomeEvent1.class)
                    .transition("transition 1")
                        .from(TestStates.State1)
                        .action(params -> processAction("pre-1", params))
                        .to(TestStates.State2)
                        .action(params -> processAction("post-2", params))
                        .mutate((testData) -> testData.count++)
                        .action(params -> processAction("post-3", params))
                        .end()

                    .transition("transition 2")
                        .from(TestStates.State2)
                        .to(TestStates.State3)
                        .end()

                    .transition("transition 3")
                        .from(TestStates.State3)
                        .action(params -> processAction("pre-4", params))
                        .mutate((testData) -> testData.count--)
                        .to(TestStates.State1)
                        .action(params -> processAction("post-5", params))
                        .mutate((testData) -> testData.count++)
                        .action(params -> processAction("post-6", params))
                        .end()

                .when(SomeEvent2.class)
                    .transition("transition 1")
                    .transition("transition 2")
                    .transition("transition 3")

                .when(SomeEvent3.class, (e) -> e.value <= 9)
                    .transition("transition 1")

                .when(SomeEvent3.class, (e) -> e.value == 10)
                    .transition("transition 2")

                .when(SomeEvent4.class)
                    .transition("transition 2")

                .when(SomeEvent4.class)
                    .transition("transition 4")
                    .from(TestStates.State3)
                    .to(TestStates.State1)
                    .action(params -> false) // always fail.
                    .end()
            );
        }
    }
}
