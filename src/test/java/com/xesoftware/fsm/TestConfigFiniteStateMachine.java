package com.xesoftware.fsm;

/**
 * Created by Phil on 15/07/2016.
 */
public abstract class TestConfigFiniteStateMachine
{
    // states
    enum TestStates {
        State1, State2, State3
    }

    interface ImmutableTestData {
        int getCount();
    }

    static abstract class SomeEvent {
        final TestStates expectedStartState;
        final TestStates expectedEndState;
        final int value;

        public SomeEvent(TestStates expectedStartState, TestStates expectedEndState, int value) {
            this.expectedStartState = expectedStartState;
            this.expectedEndState = expectedEndState;
            this.value = value;
        }

        public abstract String getMyEventName();
    }

    static class SomeEvent0 extends SomeEvent {
        public SomeEvent0(TestStates expectedStartState, TestStates expectedEndState, int value) {
            super(expectedStartState, expectedEndState, value);
        }

        public String getMyEventName() {
            return "SomeEvent0";
        }
    }

    static class SomeEvent1 extends SomeEvent {
        public SomeEvent1(TestStates expectedStartState, TestStates expectedEndState, int value) {
            super(expectedStartState, expectedEndState, value);
        }

        public String getMyEventName() {
            return "SomeEvent1";
        }
    }

    static class SomeEvent2 extends SomeEvent {
        public SomeEvent2(TestStates expectedStartState, TestStates expectedEndState, int value) {
            super(expectedStartState, expectedEndState, value);
        }

        public String getMyEventName() {
            return "SomeEvent2";
        }
    }

    static class SomeEvent3 extends SomeEvent {
        public SomeEvent3(TestStates expectedStartState, TestStates expectedEndState, int value) {
            super(expectedStartState, expectedEndState, value);
        }

        public String getMyEventName() {
            return "SomeEvent3";
        }
    }

    static class SomeEvent4 extends SomeEvent {
        public SomeEvent4(TestStates expectedStartState, TestStates expectedEndState, int value) {
            super(expectedStartState, expectedEndState, value);
        }

        public String getMyEventName() {
            return "SomeEvent4";
        }
    }

    static class TestTestData implements ImmutableTestData {
        public int count = 0;

        public int getCount() {
            return count;
        }

        @Override
        public String toString() {
            return "" + count;
        }
    }
}
