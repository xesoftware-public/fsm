package com.xesoftware.fsm;

import org.junit.Test;

public class FiniteStateMachineConstructorTest {

    @Test(expected = NullPointerException.class)
    public void constructor_Null_Initial_State() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates,
                TestConfigFiniteStateMachine.SomeEvent,
                TestConfigFiniteStateMachine.TestTestData,
                TestConfigFiniteStateMachine.ImmutableTestData>(
                null,
                new TestConfigFiniteStateMachine.TestTestData(),
                (builder) -> builder
                    .when(TestConfigFiniteStateMachine.SomeEvent.class)
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void constructor_Null_DAta() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates,
                TestConfigFiniteStateMachine.SomeEvent,
                TestConfigFiniteStateMachine.TestTestData,
                TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1,
                null,
                (builder) -> builder
                        .when(TestConfigFiniteStateMachine.SomeEvent.class)
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void constructor_Null_Builder() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates,
                TestConfigFiniteStateMachine.SomeEvent,
                TestConfigFiniteStateMachine.TestTestData,
                TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1,
                new TestConfigFiniteStateMachine.TestTestData(), null
        ) {
        };
    }

    @Test(expected = IllegalStateException.class)
    public void builder_Fail_Not_Concrete_Event_Class() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(TestConfigFiniteStateMachine.SomeEvent.class)
        ) {
        };
    }

    @Test(expected = IllegalStateException.class)
    public void builder_Fail_Duplicate_Sequence() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(TestConfigFiniteStateMachine.SomeEvent1.class)
                .transition("test1")
                .from(TestConfigFiniteStateMachine.TestStates.State1).to(TestConfigFiniteStateMachine.TestStates.State2).end()
                .transition("test2")
                .from(TestConfigFiniteStateMachine.TestStates.State1).to(TestConfigFiniteStateMachine.TestStates.State2).end()
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void builder_Fail_No_Transition_Name() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(TestConfigFiniteStateMachine.SomeEvent1.class)
                .transition(null)
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void builder_Fail_Null_When() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(null)
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void builder_Fail_Null_When_Predicate() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(null, (e) -> true)
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void builder_Fail_Null_From() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(TestConfigFiniteStateMachine.SomeEvent1.class)
                .transition("Fail_Null_From")
                .from(null).end()
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void builder_Fail_Null_To() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(TestConfigFiniteStateMachine.SomeEvent1.class)
                .transition("Fail_Null_To")
                .from(TestConfigFiniteStateMachine.TestStates.State1)
                .to(null).end()
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void builder_Fail_Null_Action() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(TestConfigFiniteStateMachine.SomeEvent1.class)
                .transition("Fail_Null_To")
                .from(TestConfigFiniteStateMachine.TestStates.State1)
                .to(TestConfigFiniteStateMachine.TestStates.State1)
                .action(null).end()
        ) {
        };
    }

    @Test(expected = NullPointerException.class)
    public void builder_Fail_Null_Mutate() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(TestConfigFiniteStateMachine.SomeEvent1.class)
                .transition("Fail_Null_To")
                .from(TestConfigFiniteStateMachine.TestStates.State1)
                .to(TestConfigFiniteStateMachine.TestStates.State1)
                .mutate(null).end()
        ) {
        };
    }

    @Test(expected = IllegalStateException.class)
    public void builder_Fail_Forced_To() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> ((FiniteStateMachineBase.Builder)builder)
                .to(TestConfigFiniteStateMachine.TestStates.State1).end()
        ) {
        };
    }

    @Test(expected = IllegalStateException.class)
    public void builder_Fail_Forced_Action() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> ((FiniteStateMachineBase.Builder)builder)
                .action((p) -> true).end()
        ) {
        };
    }

    @Test(expected = IllegalStateException.class)
    public void builder_Fail_Forced_Mutate() {
        new FiniteStateMachineBase<
                TestConfigFiniteStateMachine.TestStates,
                TestConfigFiniteStateMachine.SomeEvent,
                TestConfigFiniteStateMachine.TestTestData,
                TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> ((FiniteStateMachineBase.Builder)builder)
                .mutate(data -> data.toString()).end()
        ) {
        };
    }

    @Test(expected = IllegalStateException.class)
    public void builder_Fail_Forced_End() {
        new FiniteStateMachineBase<
                TestConfigFiniteStateMachine.TestStates,
                TestConfigFiniteStateMachine.SomeEvent,
                TestConfigFiniteStateMachine.TestTestData,
                TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> ((FiniteStateMachineBase.Builder)builder)
                .end()
        ) {
        };
    }

    @Test(expected = IllegalStateException.class)
    public void builder_Fail_Forced_End_No_To() {
        new FiniteStateMachineBase<TestConfigFiniteStateMachine.TestStates, TestConfigFiniteStateMachine.SomeEvent, TestConfigFiniteStateMachine.TestTestData, TestConfigFiniteStateMachine.ImmutableTestData>(
                TestConfigFiniteStateMachine.TestStates.State1, new TestConfigFiniteStateMachine.TestTestData(), (builder) -> builder
                .when(TestConfigFiniteStateMachine.SomeEvent1.class)
                .transition("transition")
                .from(TestConfigFiniteStateMachine.TestStates.State1)
                .end()
        ) {
        };
    }
}
